//
//  SpriteSpawner.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 12/06/15.
//  Copyright (c) 2015 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import UIKit

class TankSpawner {
    
    static let InitialArmySize = 10
    let sceneSize: CGSize
    
    init(sceneSize: CGSize) {
        
        self.sceneSize = sceneSize
        
    }
    
    //return nil if too many tanks on the field
    func generateTank(blueCount: Int, redCount: Int) -> Tank? {
        
        let jointCount = (blueCount,redCount)
        
        switch jointCount {
            
        case let (b,r) where b >= TankSpawner.InitialArmySize && r >= TankSpawner.InitialArmySize :
            
            return nil
            
        case let (b,r) where b < TankSpawner.InitialArmySize && r < TankSpawner.InitialArmySize :
            
            return self.generateRandomArmy()
            
        case let(b,_) where b < TankSpawner.InitialArmySize :
            
            return self.generateBlueArmy(1).first
            
        default : // let(_,r) where r < self.initialArmySize :
            
            return self.generateRedArmy(1).first
            
        }
    }
    
    func generateRedArmy() -> [Tank] {
        return self.generateRedArmy(TankSpawner.InitialArmySize)
    }
    
    func generateBlueArmy() -> [Tank] {
        return self.generateBlueArmy(TankSpawner.InitialArmySize)
    }
    
    //MARK: Internals
    
    private func generateRedArmy(count: Int) -> [Tank] {
        
        let rect = self.redArmySpawnBoundaries()
        return self.generateTanks(count, rect: rect, army: Army.Red, rotation: CGFloat(-M_PI/2))
    }
    
    private func generateBlueArmy(count: Int) -> [Tank] {
        
        let rect = self.blueArmySpawnBoundaries()
        return self.generateTanks(count, rect: rect, army: Army.Blue, rotation: CGFloat(M_PI/2))
    }
    
    private func generateTanks(count: Int, rect: CGRect, army: Army, rotation: CGFloat) -> [Tank] {
        
        var sprites = [Tank]()
        
        for x in 1...count {
            
            let sprite = Tank(army: army)
            sprite.position = GameRandomizer.randomScenePosition(rect)
            sprite.zRotation = rotation
            sprites.append(sprite)
        }
        
        return sprites
    }
    
    private func generateRandomArmy() -> Tank {
        
        if arc4random_uniform(2)%2 == 0 {
            return self.generateBlueArmy(1).first!
        }
        else {
            return self.generateRedArmy(1).first!
        }
    }
    
    //left screen quarter
    private func redArmySpawnBoundaries() -> CGRect {
        return CGRectMake(0, 0, self.sceneSize.width*0.25, self.sceneSize.height);
    }
    
    //right screen quarter
    private func blueArmySpawnBoundaries() -> CGRect {
        return CGRectMake(self.sceneSize.width*0.75, 0, self.sceneSize.width*0.25, self.sceneSize.height)
    }
}