//
//  GameEngine.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 12/06/15.
//  Copyright (c) 2015 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class GameEngine: NSObject, SKPhysicsContactDelegate {
    
    let spawner: TankSpawner

    var redArmy: [Tank]
    var blueArmy: [Tank]

    init(sceneSize:CGSize){
        
        self.spawner = TankSpawner(sceneSize: sceneSize)
        self.redArmy = self.spawner.generateRedArmy()
        self.blueArmy = self.spawner.generateBlueArmy()
        
    }
    
    func update() {
        
        self.redArmy.map{$0.updateTarget(self.blueArmy)}
        self.blueArmy.map{$0.updateTarget(self.redArmy)}

    }
    
    func generateNewTanks(count: Int) -> [Tank] {
        
        var generatedTanks = [Tank]()
        
        for x in 0..<count {
            
            let possibleTank = self.spawner.generateTank(self.blueArmy.count, redCount: self.redArmy.count)
            
            if let tank = possibleTank {
                generatedTanks.append(tank)
            }
        }
        
        self.redArmy.extend(generatedTanks.filter{$0.army == Army.Red})
        self.blueArmy.extend(generatedTanks.filter{$0.army == Army.Blue})
        
        return generatedTanks
    }
    
    //MARK: Collision handling
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        
        var body1 = contact.bodyA;
        var body2 = contact.bodyB;
        
        //missile & tank contact
        
        if let tank = body1.node as? Tank {
            if let missile = body2.node as? Missile {
                self.tankMissileCollision(tank, missile: missile, point: contact.contactPoint)
            }
        }
        else if let tank = body2.node as? Tank {
            if let missile = body1.node as? Missile {
                self.tankMissileCollision(tank, missile: missile, point: contact.contactPoint)
            }
        }
    }
    
    //MARK: Helpers
    
    func tankMissileCollision(tank: Tank, missile: Missile, point: CGPoint) {
        
        if tank.isEnemyMissile(missile) == false {
            return
        }
        
        if tank.takeHit(missile.damage) == true {
            //one of them will do nothing
            self.redArmy = self.redArmy.filter{$0 != tank}
            self.blueArmy = self.blueArmy.filter{$0 != tank}
        }
        
        missile.removeFromParent()
    }
    
}