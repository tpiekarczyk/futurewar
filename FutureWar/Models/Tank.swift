//
//  Tank.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import SpriteKit

struct TankNSCodingKeys
{
    static let Army = "army"
    static let HealthBar = "healthBar"
}

class Tank: SKSpriteNode {

    static let TankSize = CGSizeMake(40,40)
    static let TurnRadius: CGFloat = 30
    static let MissileRange: CGFloat = 700
    static let AcquiringTargetActionKey = "AcquiringTargetActionKey"
    
    let army: Army
    var health: CGFloat = 100
    
    var destroyed = false //destroyed but animation still in progress
    var currentTarget: Tank?
    var firedMissile: Missile?
    var healthBar: SKShapeNode
    
    //MARK: NSCoding
    required init?(coder aDecoder: NSCoder) {
        
        self.army = Army(rawValue: aDecoder.decodeIntForKey(TankNSCodingKeys.Army))!
        self.healthBar = aDecoder.decodeObjectForKey(TankNSCodingKeys.HealthBar) as! SKShapeNode
        super.init(coder: aDecoder)
    }
    
    override func encodeWithCoder(aCoder: NSCoder) {
        
        super.encodeWithCoder(aCoder)
        aCoder.encodeInt(self.army.rawValue, forKey:TankNSCodingKeys.Army)
        aCoder.encodeObject(self.healthBar, forKey:TankNSCodingKeys.HealthBar)
    }
    
    //MARK:
    init(army: Army) {
        
        self.army = army
        let texture = SKTexture(imageNamed: "Tank")
        
        self.healthBar = SKShapeNode(path: Tank.healthBarPath(Tank.maxHealth(), radius: texture.size().width))
        self.healthBar.fillColor = UIColor.redColor().colorWithAlphaComponent(0.5)
        self.healthBar.zPosition = 1
        
        super.init(texture: texture, color: self.army.color(), size: texture.size())
        self.name = self.army.name()
        self.colorBlendFactor = 0.25
        
        let turret = SKSpriteNode(imageNamed:"TankTurret")
        turret.color = self.army.color()
        turret.colorBlendFactor = 0.25
        turret.position = CGPointMake(0, 20)
        self.addChild(turret)
        
        let body = SKPhysicsBody(rectangleOfSize: texture.size(), center: CGPointZero)
        body.friction = 1.0
        
        body.collisionBitMask = HitTestCategories.TankCategory
        body.categoryBitMask = HitTestCategories.TankCategory
        self.physicsBody = body
        
        self.addChild(self.healthBar)
    }
    
    func fire() -> Missile {
        return Missile(army: self.army, position: self.position, rotation: self.zRotation, range: Tank.MissileRange)
    }
    
    /**
    @return true if destroyed
    */
    func takeHit(damage: CGFloat) -> Bool {

        self.health = self.health - damage
        
        if self.health <= 0 {
            
            self.healthBar.path = Tank.healthBarPath(0, radius: self.texture!.size().width)
            self.explode({ () -> Void in
                self.removeFromParent()
            })
            return true
        }
        else {
            self.healthBar.path = Tank.healthBarPath(self.health, radius: self.texture!.size().width)
            return false
        }
        
    }
    
    func explode(completion: () -> Void) {

        self.destroyed = true
        let explosionNode = SKSpriteNode(color: UIColor.clearColor(), size: CGSizeMake(50, 50))
        explosionNode.xScale = 2
        explosionNode.yScale = 2
        
        let explosion = self.explosionAction()
        self.removeAllActions()
        
        //no longer existing as a body
        self.physicsBody = nil
        
        explosionNode.runAction(explosion, completion: { () -> Void in
            
            let fadeOutAction = SKAction.fadeAlphaTo(0, duration: 1)
            
            self.runAction(fadeOutAction,completion: completion)
            
        })
        
        self.addChild(explosionNode)
        
    }
    
    class func healthBarPath(health: CGFloat, var radius: CGFloat) -> CGPath {
        
        radius = radius*health/Tank.maxHealth()
        return UIBezierPath(ovalInRect: CGRectMake(-radius/2,-radius/2,radius,radius)).CGPath
    }
    
    class func maxHealth() -> CGFloat {
    
        return 100
    }
    
    func explosionAction() -> SKAction {
        
        let atlas = SKTextureAtlas(named: "Explosion")
        
        var textures : [SKTexture] = []
        var textureNames : [String] = atlas.textureNames as! [String]
        textureNames.sort({$0 < $1})
        
        for name in textureNames {
            textures.append(SKTexture(imageNamed:name))
        }
        
        return SKAction.animateWithTextures(textures, timePerFrame: 0.05)
    }
    
    func isEnemyMissile(missile: Missile) -> Bool {
        
        if (missile.army != self.army) {
            return true
        }
        else {
            return false
        }
    }
    
    //MARK: primitive AI
    
    func updateTarget(tanks: [Tank]) {

        if self.destroyed == true {
            return
        }

        let workingTanks = tanks.filter({$0.destroyed == false})
        
        if workingTanks.isEmpty == true {
            
            self.removeActionForKey(Tank.AcquiringTargetActionKey)
            return
        }
        
        let sortedByDistance = workingTanks.sorted {
            CGPointDistance(self.position,$0.position) < CGPointDistance(self.position,$1.position)
        }
        
        if self.shouldAdjustPath(sortedByDistance.first!) == true {
        
            self.currentTarget = sortedByDistance.first
            let path = self.targetPath(self.currentTarget)
            self.runAction(SKAction.followPath(path!, asOffset: false, orientToPath: true, duration: 10), withKey: Tank.AcquiringTargetActionKey)
        }
        
        self.fireIfTargetVisible(self.zRotation,distance:Tank.MissileRange)
    }
    

    func shouldAdjustPath(nearestTank: Tank) -> Bool {

        //there is a different tank closer
        if self.currentTarget != nearestTank {
            return true
        }
        
        //if not moving toward target
        if self.actionForKey(Tank.AcquiringTargetActionKey) == nil {
            return true
        }
        
        return false
    }
    
    func targetPath(tank: Tank?) -> CGPathRef? {
    
        if let existingTank = tank {
            
            let range = Tank.MissileRange
            
            var path = CGPathCreateMutable()
            CGPathMoveToPoint(path, nil, self.position.x, self.position.y)
            
            CGPathAddQuadCurveToPoint(path, nil, self.position.x, self.position.y, existingTank.position.x, existingTank.position.y)
            
            return path
        }
        
        return nil
    }
    
    func hasFiredMissile() -> Bool {
       
        if let missile = self.firedMissile {
            
            if self.firedMissile?.scene == nil {
                return false
            }
            else {
                return true
            }
        }
        
        return false
    }
    
    func fireIfTargetVisible(angle: CGFloat, distance: CGFloat) {
        
        //only a single missile can exist
        if self.hasFiredMissile() == true {
            return
        }
            
        self.firedMissile = nil
        
        let rayStart = self.position;

        let angleD = Double(angle) + M_PI/2
        let cosA = CGFloat(cos(angleD))
        let sinA = CGFloat(sin(angleD))
        let rayEnd = CGPointMake(rayStart.x+distance*cosA, rayStart.y+distance*sinA)
        
        let body =  self.scene!.physicsWorld.bodyAlongRayStart(rayStart, end: rayEnd)
        
        if let enemyTank = body?.node as? Tank {
            let missile = self.fire()
            self.firedMissile = missile
            self.scene!.addChild(missile)
        }
    }
}