//
//  Missile.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import SpriteKit

struct MissileNSCodingKeys
{
    static let Range = "range"
    static let Army = "army"
    static let Damage = "damage"
}

class Missile: SKSpriteNode {
    
    static let MaxDamage = 100
    static let MissileLifeTime: NSTimeInterval = 3
    static let MissileFadeOutPercent: CGFloat = 0.8 //after what portion of missileRange should fade out kick in
    
    let army: Army
    var range: CGFloat
    var damage: CGFloat
    
    //MARK: NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        
        self.range = CGFloat(aDecoder.decodeFloatForKey(MissileNSCodingKeys.Range))
        self.army = Army(rawValue: aDecoder.decodeIntForKey(MissileNSCodingKeys.Army))!
        self.damage = CGFloat(aDecoder.decodeFloatForKey(MissileNSCodingKeys.Damage))
        
        super.init(coder: aDecoder)
    }
    
    override func encodeWithCoder(aCoder: NSCoder) {

        super.encodeWithCoder(aCoder)
        aCoder.encodeFloat(Float(self.range), forKey:MissileNSCodingKeys.Range)
        aCoder.encodeInt(self.army.rawValue, forKey:MissileNSCodingKeys.Army)
        aCoder.encodeFloat(Float(self.damage), forKey:MissileNSCodingKeys.Damage)
    }
    
    //MARK:
    init(army: Army, position: CGPoint, rotation: CGFloat, range: CGFloat) {
        
        self.army = army
        self.range = range
        self.damage = Missile.randomDamage()
        
        let texture = SKTexture(imageNamed: "Missile")
        super.init(texture: texture, color: army.color(), size: texture.size())
        
        self.colorBlendFactor = 0.5
        
        let body = SKPhysicsBody(rectangleOfSize: self.size, center: CGPointZero)
        body.categoryBitMask = HitTestCategories.MissileCategory
        body.contactTestBitMask = HitTestCategories.TankCategory
        body.collisionBitMask = 0
        
        self.physicsBody = body
        self.position = position
        self.zRotation = rotation
        
        let movementAction = self.movementAction(rotation)
        let movementAndFadeOutAction = self.movementAndFadeoutAction(rotation)
        let removeAction = SKAction.removeFromParent()
        
        self.runAction(SKAction.sequence([movementAction,movementAndFadeOutAction,removeAction]))

        let emitter = SKEmitterNode(fileNamed: "MissileSmoke")
        emitter.zRotation = CGFloat(M_PI)
        emitter.position.y = -10
        self.addChild(emitter)
    }
    
    //MARK: Action helpers
    func movementAction(rotation: CGFloat) -> SKAction {
        let missileDistance : CGFloat = self.range * Missile.MissileFadeOutPercent
        let duration : NSTimeInterval = Missile.MissileLifeTime * NSTimeInterval(Missile.MissileFadeOutPercent)
        
        let vector = CGVectorMultiply(self.movementDirectionVector(rotation),missileDistance)
        return SKAction.moveBy(vector, duration: duration);
    }
    
    func movementAndFadeoutAction(rotation: CGFloat) -> SKAction {
        let factor: CGFloat = 1-Missile.MissileFadeOutPercent
        let missileDistance: CGFloat = self.range * factor
        let duration: NSTimeInterval = Missile.MissileLifeTime * NSTimeInterval(factor)
        
        let vector = CGVectorMultiply(self.movementDirectionVector(rotation), missileDistance)
        
        let moveAction = SKAction.moveBy(vector, duration: duration)
        let fadeOutAction = SKAction.fadeOutWithDuration(duration)
        
        return SKAction.group([moveAction,fadeOutAction])
    }
    
    func movementDirectionVector(rotation: CGFloat) -> CGVector {
        
        let dx = -sin(Float(rotation))
        let dy = cos(Float(rotation))
        return CGVector(dx:CGFloat(dx), dy:CGFloat(dy))
    }
    
    private class func randomDamage() -> CGFloat {
        return 10*CGFloat(arc4random_uniform(UInt32(Missile.MaxDamage/10)))
    }
}
