//
//  AppDelegate.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
        return true
    }
}

