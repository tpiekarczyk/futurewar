//
//  GameScene.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import SpriteKit
import Foundation

class GameScene: SKScene {
    
    var engine: GameEngine!
    var redCountLabel: SKLabelNode?
    var blueCountLabel: SKLabelNode?

    
    override func didMoveToView(view: SKView) {

        self.setupLabels()
        self.engine = GameEngine(sceneSize: self.size)
    
        self.addChildren(self.engine.blueArmy)
        self.addChildren(self.engine.redArmy)
            
        let body = SKPhysicsBody(edgeLoopFromRect:CGRect(origin: CGPoint(x: 0, y: 0), size: self.scene!.size))
            
        self.physicsBody = body
        self.physicsWorld.gravity = CGVector.zeroVector
        self.physicsWorld.contactDelegate = self.engine
        
        super.didMoveToView(view)
    }

    func setupLabels() {
        
        let prompt = SKLabelNode(text: "Tap anywhere to spawn additional tanks")
        prompt.position = CGPointMake(self.size.width*0.5,self.size.height-50)
        prompt.fontColor = UIColor.blackColor()
        
        self.addChild(prompt)
        
        self.redCountLabel = SKLabelNode()
        self.redCountLabel?.fontColor = Army.Red.color()
        self.blueCountLabel = SKLabelNode()
        self.blueCountLabel?.fontColor = Army.Blue.color()
        
        self.redCountLabel!.position = CGPointMake(self.size.width*0.25, 10)
        self.blueCountLabel!.position = CGPointMake(self.size.width*0.75, 10)
        
        self.addChild(self.redCountLabel!)
        self.addChild(self.blueCountLabel!)
    }
    
    override func update(currentTime: CFTimeInterval) {
        
        self.engine.update()
        self.updateCountLabels()
    }
    
    func updateCountLabels() {
        self.redCountLabel!.text = "Red tanks \(self.engine.redArmy.count)"
        self.blueCountLabel!.text = "Blue tanks \(self.engine.blueArmy.count)"
    }
    
    func indicateTooManySprites() -> Void {
        
        //still animating - skip
        if self.blueCountLabel!.hasActions() || self.redCountLabel!.hasActions() {
            return
        }
        
        let scaleUp = SKAction.scaleTo(1.4, duration: 0.3)
        let scaleDown = SKAction.scaleTo(1, duration: 0.3)
        let sequence = SKAction.sequence([scaleUp,scaleDown])
        
        self.blueCountLabel!.runAction(sequence)
        self.redCountLabel!.runAction(sequence)

    }
    
    //MARK: Additional sprite generation
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        let generatedTanks = self.engine.generateNewTanks(touches.count)
        
        if generatedTanks.count == 0 {
            self.indicateTooManySprites()
        }
        else {
            self.addChildren(generatedTanks)
            self.updateCountLabels()
        }
    }
}
