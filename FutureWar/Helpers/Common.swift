//
//  Constants.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import UIKit

enum Army: Int32 {
    case Red
    case Blue
    
    func color() -> UIColor {
        
        switch self {
            
        case .Red:
            return UIColor.redColor()
        case .Blue:
            return UIColor.blueColor()
        }
    }
    
    func name() -> String {
        
        switch self {
            
        case .Red:
            return "Red"
        case .Blue:
            return "Blue"
            
        }
    }
}

func CGVectorMultiply(vector: CGVector, scalar: CGFloat) -> CGVector {
    
    return CGVectorMake(vector.dx*scalar, vector.dy*scalar)
}

func CGPointDistance(pt1: CGPoint, pt2: CGPoint) -> CGFloat
{
    return sqrt(pow(pt2.x-pt1.x,2)+pow(pt2.y-pt1.y,2));
}

struct HitTestCategories {
    static let TankCategory: UInt32 = 1
    static let MissileCategory: UInt32 = 2
}