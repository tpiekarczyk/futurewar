//
//  GameSceneExtention.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 12/06/15.
//  Copyright (c) 2015 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import SpriteKit

extension SKScene {
    
    func addChildren(children: [SKNode]) {
        
        for sprite in children {
            self.addChild(sprite)
        }
        
    }
}