//
//  GameRandomizer.swift
//  FutureWar
//
//  Created by Tomasz Piekarczyk on 30/08/14.
//  Copyright (c) 2014 Tomasz Piekarczyk. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

class GameRandomizer {
    
    class func randomScenePosition(rect : CGRect) -> CGPoint {

        let x = rect.origin.x + CGFloat(arc4random_uniform(UInt32(rect.size.width)))
        let y = rect.origin.y + CGFloat(arc4random_uniform(UInt32(rect.size.height)))
        
        return CGPointMake(x, y)
    }
    
    class func randomColor() -> UIColor {

        let r = CGFloat(arc4random_uniform(255))/255.0
        let g = CGFloat(arc4random_uniform(255))/255.0
        let b = CGFloat(arc4random_uniform(255))/255.0
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
}