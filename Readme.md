
Simple Sprite Kit demo in a form of 2 tank divisions fighting against each other.

## Requirements

- The project should compile out of the box on XCode 6 beta 5. 
- No 3rd party libraries needed.

Tested on iPad 3rd generation with iOS 8 beta 5.

## Instruction

- The demo is ipad oriented (limited to landscape for simplicity)
- After running the app 20 tanks are generated and a combat is started
- Additional tanks can be spawned by tapping the screen
- Each side cannot have more than 10 tanks at each given moment
- Each tank can have only a single fired missile at any given moment (missile needs to go off screen or disappear due to limited range)

## Known Issues

- Console spamming: ```self.scene.physicsWorld.bodyAlongRayStart(rayStart, end: rayEnd)``` keeps writing `Chance` on every call - reason unknown
- Turning: At this moment there is no turning tanks magically reorient instantly to desired angle
- First frame lag - there is a slight lag on first frame - needs investigation

## Acknowledgments

All assets from http://opengameart.org
